package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import com.example.flywaydemo.domain.Model.User;
import com.example.flywaydemo.domain.Repository.Userrepo;
import com.example.flywaydemo.domain.Service.UserServiceImplement;


import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserserviceTest {

    @Mock
    Userrepo repo ;

    @InjectMocks
    UserServiceImplement serviceimplement ;

    private User mockuser_first , mockuser_second ;

    @BeforeEach
    void setup() throws Exception {
        mockuser_first = new User() ;
        mockuser_first.setUsername("mock1");
        mockuser_first.setId(200);
        mockuser_first.setfirst_name("mo");
        mockuser_first.setlast_name("ck");
        mockuser_first.setEmail("mock@gmail.com");
        mockuser_first.setCity("Bangkok");
        mockuser_first.setPostcode("10400");
        mockuser_first.setTelephone("0112345678");
        mockuser_second = new User() ;
        mockuser_second.setUsername("mock2");
        mockuser_second.setId(300);
        mockuser_second.setfirst_name("mo");
        mockuser_second.setlast_name("ck2");
        mockuser_second.setEmail("mock2@gmail.com");
        mockuser_second.setCity("Bangkok");
        mockuser_second.setPostcode("10400");
        mockuser_second.setTelephone("0112345678");
    }
    
    @DisplayName("test Userservice Create") 
    
    @Test
    public void mocktestCreateUser() {
     
        
        given(repo.save(mockuser_first)).willReturn(mockuser_first);

        System.out.println(repo);
        System.out.println(serviceimplement);

        // when -  action or the behaviour that we are going test
        User createUser = serviceimplement.create(mockuser_first);

        System.out.println(createUser);
 
        
        // then - verify the output
        assertThat(createUser).isNotNull() ;
    }

    @DisplayName("test Userservice findalluser")
    @Test
    public void mocktestFindallUser(){

        given(repo.findAll()).willReturn(List.of(mockuser_first,mockuser_second));

        // when -  action or the behaviour that we are going test
        List<User> userList = serviceimplement.findAllUser();

       for(int i =0 ; i<userList.size() ; i++) {
        System.out.println(userList.get(i).getId() + " " + userList.get(i).getUsername() + " " + userList.get(i).getfirst_name()+ " " + userList.get(i).getlast_name()+ " "
        +userList.get(i).getEmail() + " "+ userList.get(i).getCity() + " "+ userList.get(i).getPostcode()+ " " + userList.get(i).getTelephone() );
       }

        // then - verify the output
        assertThat(userList).isNotNull();
        assertThat(userList.size()).isEqualTo(2);
        for(int i =0 ; i<userList.size() ; i++) {
            assertThat(userList.get(i)).isNotNull();
           }
    
    }

    @DisplayName("test UserSerivice finduserid")
    @Test
    public void mocktestfinduserbyid() {

        given(repo.findById(200)).willReturn(Optional.of(mockuser_first)) ;
        given(repo.findById(300)).willReturn(Optional.of(mockuser_second)) ;

        User outpUser_first = serviceimplement.findUserById(mockuser_first.getId()).get() ;
        User outpUser_second = serviceimplement.findUserById(mockuser_second.getId()).get() ;

        System.out.println(outpUser_first);
        System.out.println(outpUser_second);

        assertThat(outpUser_first).isNotNull() ;
        assertThat(outpUser_second).isNotNull() ;
    }

    @DisplayName("test UserSerivice updateall") 
    @Test
    public void mocktestUpdateuser() {
        given(repo.save(mockuser_first)).willReturn(mockuser_first) ;
        given(repo.save(mockuser_second)).willReturn(mockuser_second) ;
        mockuser_first.setCity("Chiangmai");
        mockuser_first.setPostcode("10530");
        mockuser_second.setUsername("mock4");

        User updatedmockuser = serviceimplement.update(mockuser_first) ;
        User updatedmockuser_2 = serviceimplement.update(mockuser_second) ;

        System.out.println(updatedmockuser);
        System.out.println(updatedmockuser_2);

        assertThat(updatedmockuser).isNotNull();
        assertThat(updatedmockuser_2).isNotNull();
        assertThat(updatedmockuser.getCity()).isEqualTo("Chiangmai") ;
        assertThat(updatedmockuser.getPostcode()).isEqualTo("10530") ;

        assertThat(updatedmockuser_2.getUsername()).isEqualTo("mock4") ;

    }

    @DisplayName("test Userservice delete")
    @Test
    public void mocktestDeleteuser() {

        int id = 100 ;
        willDoNothing().given(repo).deleteById(id);

        // when -  action or the behaviour that we are going test
serviceimplement.delete(id);

        // then - verify the output
      verify(repo,times(1)).deleteById(id);
    }
    
}
