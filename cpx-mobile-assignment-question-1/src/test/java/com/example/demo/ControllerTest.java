package com.example.demo;

import com.example.flywaydemo.domain.Controller.UserController;
import com.example.flywaydemo.domain.Model.User;
import com.example.flywaydemo.domain.Service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@AutoConfigureMockMvc
@WebMvcTest(controllers = UserController.class)
@ContextConfiguration(classes={UserController.class})
public class ControllerTest {
    
    @Autowired
    private MockMvc mockMvctest ;

    @MockBean
    UserService userService ;

    @Autowired
    private ObjectMapper objmapper ;

    private User user_first ;
    private User user_second ;

    @BeforeEach
    void setup() throws Exception {
        user_first = new User() ;
        user_first.setUsername("mock1");
        user_first.setId(200);
        user_first.setfirst_name("mo");
        user_first.setlast_name("ck");
        user_first.setEmail("mock@gmail.com");
        user_first.setCity("Bangkok");
        user_first.setPostcode("10400");
        user_first.setTelephone("0112345678");
        user_second = new User() ;
        user_second.setUsername("mock2");
        user_second.setId(300);
        user_second.setfirst_name("mo");
        user_second.setlast_name("ck2");
        user_second.setEmail("mock2@gmail.com");
        user_second.setCity("Bangkok");
        user_second.setPostcode("10400");
        user_second.setTelephone("0112345678");
     
    
    }
    

    @DisplayName("Createdata")
    @Test
    public void mockCreatecontroller() throws Exception {
     

        given(userService.create(any(User.class)))
        .willAnswer((invocation)-> invocation.getArgument(0));


// when - action or behaviour that we are going test
ResultActions response = mockMvctest.perform(post("/user/newUser")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objmapper.writeValueAsString(user_first)));

    
// then - verify the result or output using assert statements
response.andDo(print()).
        andExpect(status().isCreated()) .andExpect(jsonPath("$.username",
        is(user_first.getUsername())))
.andExpect(jsonPath("$.first_name",
        is(user_first.getfirst_name())))
        .andExpect(jsonPath("$.last_name",
        is(user_first.getlast_name())))
.andExpect(jsonPath("$.email",
        is(user_first.getEmail())))
        .andExpect(jsonPath("$.city",
        is(user_first.getCity())))
        .andExpect(jsonPath("$.postcode",
        is(user_first.getPostcode())))
.andExpect(jsonPath("$.telephone",
        is(user_first.getTelephone())));
    }

    @DisplayName("getalldata")
    @Test
    public void mockfindAllController() throws Exception{
        // given - precondition or setup
        List<User> userList = new ArrayList<>();
      
        given(userService.findAllUser()).willReturn(userList);

        // when -  action or the behaviour that we are going test
        Mockito.when(userService.findAllUser()).thenReturn(userList);
        ResultActions response = mockMvctest.perform(get("/user/userid"));

        // then - verify the output
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()",
                        is(userList.size())));

    }

    @DisplayName("getDatabyId")
    @Test
    public void mockfinduserbyIdController() throws Exception {
        int id = 200 ;
        given(userService.findUserById(id)).willReturn(Optional.of(user_first)) ;

        ResultActions response = mockMvctest.perform(get("/user/id/{id}" , id));

        // then - verify the output
        response.andDo(print()).
        andExpect(status().isOk()) .andExpect(jsonPath("$.username",
        is(user_first.getUsername())))
.andExpect(jsonPath("$.first_name",
        is(user_first.getfirst_name())))
        .andExpect(jsonPath("$.last_name",
        is(user_first.getlast_name())))
.andExpect(jsonPath("$.email",
        is(user_first.getEmail())))
        .andExpect(jsonPath("$.city",
        is(user_first.getCity())))
        .andExpect(jsonPath("$.postcode",
        is(user_first.getPostcode())))
.andExpect(jsonPath("$.telephone",
        is(user_first.getTelephone())));


    }
    @DisplayName("UpdateAlldata")
    @Test
    public void mockUpdatecontroller() throws Exception {
        int id = 200 ;
        given(userService.findUserById(id)).willReturn(Optional.of(user_first)) ;

        given(userService.update(any(User.class)))
        .willAnswer((invocation)-> invocation.getArgument(0));
        

// when - action or behaviour that we are going test
ResultActions response = mockMvctest.perform(put("/user/id/{id}" , id)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objmapper.writeValueAsString(user_second)));

    
// then - verify the result or output using assert statements
response.andDo(print()).
        andExpect(status().isOk()) .andExpect(jsonPath("$.username",
        is(user_second.getUsername())))
.andExpect(jsonPath("$.first_name",
        is(user_second.getfirst_name())))
        .andExpect(jsonPath("$.last_name",
        is(user_second.getlast_name())))
.andExpect(jsonPath("$.email",
        is(user_second.getEmail())))
        .andExpect(jsonPath("$.city",
        is(user_second.getCity())))
        .andExpect(jsonPath("$.postcode",
        is(user_second.getPostcode())))
.andExpect(jsonPath("$.telephone",
        is(user_second.getTelephone())));

}

@DisplayName("UpdatePartialdata")
    @Test
    public void mockUpdatePartialcontroller() throws Exception {
        int id = 200 ;
        given(userService.findUserById(id)).willReturn(Optional.of(user_first)) ;

        user_first.setPostcode("91000");
        user_first.setCity("Kalasin");
        given(userService.update(any(User.class)))
        .willAnswer((invocation)-> invocation.getArgument(0));
        

// when - action or behaviour that we are going test
ResultActions response = mockMvctest.perform(patch("/user/partialid/{id}" , id)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objmapper.writeValueAsString(user_first)));

    
// then - verify the result or output using assert statements
response.andDo(print()).
        andExpect(status().isAccepted()) .andExpect(jsonPath("$.username",
        is(user_first.getUsername())))
.andExpect(jsonPath("$.first_name",
        is(user_first.getfirst_name())))
        .andExpect(jsonPath("$.last_name",
        is(user_first.getlast_name())))
.andExpect(jsonPath("$.email",
        is(user_first.getEmail())))
        .andExpect(jsonPath("$.city",
        is(user_first.getCity())))
        .andExpect(jsonPath("$.postcode",
        is(user_first.getPostcode())))
.andExpect(jsonPath("$.telephone",
        is(user_first.getTelephone())));

}
@DisplayName("UpdatePartialdata")
    @Test
    public void mockDeletecontroller() throws Exception {
        int id = 200 ;
        willDoNothing().given(userService).delete(id);

ResultActions response = mockMvctest.perform(delete("/user/id/{id}" , id));
  

    
// then - verify the result or output using assert statements
response.andDo(print()).
        andExpect(status().isOk());

}
}
