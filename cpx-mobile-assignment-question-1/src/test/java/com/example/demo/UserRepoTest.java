package com.example.demo;
import com.example.flywaydemo.domain.Model.User;
import com.example.flywaydemo.domain.Repository.Userrepo;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)


public class UserRepoTest  {

    @Mock
    private Userrepo userrepo ;


    private User user_first ;
    private User user_second ;

    @BeforeEach
    @Transactional
    void setup() throws Exception {
    user_first = new User() ;
    user_first.setUsername("mock1");
    user_first.setId(200);
    user_first.setfirst_name("mo");
    user_first.setlast_name("ck");
    user_first.setEmail("mock@gmail.com");
    user_first.setCity("Bangkok");
    user_first.setPostcode("10400");
    user_first.setTelephone("0112345678");
    user_second = new User() ;
    user_second.setUsername("mock2");
    user_second.setId(300);
    user_second.setfirst_name("mo");
    user_second.setlast_name("ck2");
    user_second.setEmail("mock2@gmail.com");
    user_second.setCity("Bangkok");
    user_second.setPostcode("10400");
    user_second.setTelephone("0112345678");
    }

    @Test
    @Order(1)
    @Rollback(value = false)
    public void mockCreateRepo() {

        given(userrepo.save(user_first)).willReturn(user_first);
        User createduser = userrepo.save(user_first);

        Assertions.assertThat(createduser).isNotNull();

    }

    
    @Test
    @Order(2)
    public void mockfindAllrepo() {
        
        given(userrepo.findAll()).willReturn(List.of(user_first));
       List<User> findusers = userrepo.findAll();

        Assertions.assertThat(findusers.size()).isGreaterThan(0);


    }

    @Test
    @Order(3)
    public void mockfindIdrepo(){
        given(userrepo.findById(200)).willReturn(Optional.of(user_first)) ;
        User finduser = userrepo.findById(200).get();

        Assertions.assertThat(finduser.getId()).isEqualTo(200);

    }

    @Test
    @Order(4)
    public void mockupdateRepo() {
        given(userrepo.findById(200)).willReturn(Optional.of(user_first)) ;

        User finduser = userrepo.findById(200).get();
        Assertions.assertThat(finduser.getId()).isEqualTo(200) ;

        finduser = user_second ;

        userrepo.save(finduser) ;

        Assertions.assertThat(finduser.getId()).isEqualTo(300) ;
    }

    @Test
    @Order(5)

    public void mockupdatePartialRepo() {
        given(userrepo.findById(300)).willReturn(Optional.of(user_second)) ;
        User finduser = userrepo.findById(300).get() ;
        Assertions.assertThat(finduser.getId()).isEqualTo(300) ;

        finduser.setCity("Chumporn");
        finduser.setPostcode("10230");

        userrepo.save(finduser);

        Assertions.assertThat(finduser.getCity()).isEqualTo("Chumporn") ;
        Assertions.assertThat(finduser.getPostcode()).isEqualTo("10230") ;
        
    }

    @Test
    @Order(6)
    public void mockdeleteRepo(){

        given(userrepo.findById(300)).willReturn(Optional.of(user_second)) ;
        User finduser = userrepo.findById(300).get();

        userrepo.delete(finduser);

        User mockuser = null;

        Optional<User> optionalUser= userrepo.finduserbyUserName("mock2");

        if(optionalUser.isPresent()){
            mockuser = optionalUser.get();
        }

        Assertions.assertThat(mockuser).isNull();
    }
}
