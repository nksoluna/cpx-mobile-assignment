CREATE TABLE userinfo (
    userid serial not null primary key, 
    username varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    postcode varchar(255) NOT NULL,
    telephone Varchar(255) NOT NULL
);
