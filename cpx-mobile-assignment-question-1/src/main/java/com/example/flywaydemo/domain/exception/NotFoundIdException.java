package com.example.flywaydemo.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundIdException extends RuntimeException  {

    public NotFoundIdException(String message) {
        super(message);
    }
}