package com.example.flywaydemo.domain.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.flywaydemo.domain.Service.WorkerService;

@RestController
@RequestMapping(value = "/kafka")
public class ProductionController {

	@Autowired
	private WorkerService worker;

	@GetMapping(value = "/topic")
	public void sendMessageToKafkaTopic(@RequestParam("message") String message) {
		this.worker.produce(message);
	}
}
