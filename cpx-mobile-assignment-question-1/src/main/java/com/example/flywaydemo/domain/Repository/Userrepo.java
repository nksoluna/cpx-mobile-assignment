package com.example.flywaydemo.domain.Repository;
import com.example.flywaydemo.domain.Model.User;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Userrepo extends JpaRepository<User,Integer> {

    @Query(value = "SELECT * from userinfo WHERE email = ?1", nativeQuery = true)
    Optional<User> finduserbyEmail(String email);

    @Query(value = "SELECT * from userinfo WHERE username = ?1" ,nativeQuery = true)
    Optional<User> finduserbyUserName(String username) ;

   
    
}
