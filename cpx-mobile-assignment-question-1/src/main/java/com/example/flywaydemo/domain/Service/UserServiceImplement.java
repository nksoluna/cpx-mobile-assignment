package com.example.flywaydemo.domain.Service;
import com.example.flywaydemo.domain.Model.User;
import com.example.flywaydemo.domain.Repository.Userrepo;
import com.example.flywaydemo.domain.exception.CannotConnectdbException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

// Annotation
@Service
public class UserServiceImplement implements UserService {


    @Autowired
    private Userrepo repository;

    @Override
    @Cacheable(value="user", unless = "#result == null")  
    public List<User> findAllUser(){
        try {
            return repository.findAll();
        } catch (RuntimeException e) {
          throw new CannotConnectdbException("cannot connect Database");
        }
       
    }

    @Override
    @Cacheable(value="user",key = "#id", unless = "#result == null")  
    public Optional<User> findUserById(int id){
        try {
        
        return repository.findById(id);
        }
        catch (RuntimeException e) {
          throw new CannotConnectdbException("cannot connect Database");
        }
    
    }

    @Override
   
    public User create(User user){
        try {
        
            return repository.save(user);
            }
            catch (RuntimeException e) {
              throw new CannotConnectdbException("cannot connect Database");
            }
        
        
    }

    @Override
    @CachePut(value = "user")
    public User update(User user){
        try {
        
            return repository.save(user);
            }
            catch (RuntimeException e) {
              throw new CannotConnectdbException("cannot connect Database");
            }
        
    }

    
    @Override
    @CacheEvict(value = "user", allEntries = true)
    public void delete(int id){
        try {
            repository.deleteById(id);
            }
            catch (RuntimeException e) {
              throw new CannotConnectdbException("cannot connect Database");
            }
    }

    @Override
    @Cacheable(value="user",key = "#username", unless = "#result == null")  
    public Optional<User> findByUser(String username){
        try {
            return repository.finduserbyUserName(username);

            }
            catch (RuntimeException e) {
              throw new CannotConnectdbException("cannot connect Database");
            }
    }

    @Override
    @CachePut(value = "user")
    public User updatePartial(User user) {
        try {
        
            return repository.save(user);
            }
            catch (RuntimeException e) {
              throw new CannotConnectdbException("cannot connect Database");
            }
    }
}
