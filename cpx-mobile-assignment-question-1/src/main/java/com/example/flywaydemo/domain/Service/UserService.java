package com.example.flywaydemo.domain.Service;
import com.example.flywaydemo.domain.Model.User;
import java.util.List;
import java.util.Optional;



public interface UserService {
    List<User> findAllUser();
    Optional<User> findUserById(int id);
    User create(User user);
    User update(User user);
    void delete(int id);
    User updatePartial(User user) ;
    Optional<User> findByUser(String username);
}
