package com.example.flywaydemo.domain.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;



@Service
public class WorkerService {
    private static final Logger logger = LogManager.getFormatterLogger(WorkerService.class) ;
	private static String TOPIC = "kafka";

	@Autowired
	private KafkaTemplate<String, String> testKafka;

	public void produce(String data) {
		logger.info("Produce Topic: %s - Message: %s and welcome messsage is", TOPIC, data);
        
		this.testKafka.send(TOPIC, data);
	}

}


    

