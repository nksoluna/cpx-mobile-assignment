package com.example.flywaydemo.domain.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BackendController {

    @RequestMapping("/callbackend")
    public String call(){
         return "Hello Backend Team" ;
    }
}
