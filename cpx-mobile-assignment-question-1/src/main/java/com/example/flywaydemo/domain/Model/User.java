package com.example.flywaydemo.domain.Model;
 
import javax.persistence.*;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


 
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "userinfo")
 
public class User {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userid;
 

    @Column(unique = true)
    @Size(min = 1, max = 100)
    private String username;
    @Size(max = 50)
    private String first_name;
    @Size(max = 50)
    private String last_name;
    @Size(max = 50)
    private String email;
    @Size(max = 50)
    private String city;
    @Size(max = 50)
    private String postcode;
    @Size(max = 50)
    private String telephone;
 
   

   

    public int getId() {
        return userid;
    }
 
    public void setId(int userid) {
        this.userid = userid;
    }
 
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getfirst_name() {
        return first_name;
    }
 
    public void setfirst_name(String first_name) {
        this.first_name = first_name;
    }
 
    public String getlast_name() {
        return last_name;
    }
 
    public void setlast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setCity(String city) {
        this.city = city;
    }
 
    public String getCity() {
        return city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
 
    public String getPostcode() {
        return postcode;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
 
    public String getTelephone() {
        return telephone;
    }
}