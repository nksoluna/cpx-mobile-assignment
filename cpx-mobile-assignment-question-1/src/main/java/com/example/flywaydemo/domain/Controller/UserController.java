package com.example.flywaydemo.domain.Controller;

import com.example.flywaydemo.domain.Service.UserService;
import com.example.flywaydemo.domain.exception.NotFoundIdException;
import com.example.flywaydemo.domain.exception.NotfoundUsernameException;
import com.example.flywaydemo.domain.exception.RequiredDataException;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.flywaydemo.domain.Model.*;
import org.springframework.web.bind.annotation.PutMapping;





@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping(value = "/userid")
    public ResponseEntity<List<User>> findAll() {
        List<User> list = userService.findAllUser();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/id/{id}")
    public User findById(@PathVariable("id") Integer id) throws NotFoundIdException {
        
 
            Optional<User> userdata = userService.findUserById(id);
       
      try {
        return userdata.get();
      } catch (RuntimeException e) {
        throw new NotFoundIdException( "ID : " + id + " not found" ) ;

      }
  
    }

    @GetMapping(value = "/username/{username}")
    public User findById(@PathVariable("username") String username) throws NotFoundIdException {
        
 
            Optional<User> userdata = userService.findByUser(username);
       
      try {
        return userdata.get();
      } catch (RuntimeException e) {
        throw new NotfoundUsernameException( "User : " + username + " not found" ) ;

      }
  
    }

    @PostMapping(value = "/newUser")
    public ResponseEntity<User> create(@RequestBody User user) throws RequiredDataException {
        if(user.getUsername() == null
        || user.getfirst_name() == null || user.getlast_name() == null || user.getEmail() == null
        || user.getCity() == null || user.getPostcode() == null || user.getPostcode() == null 
    || user.getTelephone() == null) {
            String errorMessage = "" ;
        if(user.getUsername() == null) {
         errorMessage =  errorMessage + "Please Enter Username" ;
        }
        if(user.getfirst_name() == null) {
            errorMessage =  errorMessage + "Please enter your Firstname" ;
           }
           if(user.getlast_name() == null) {
            errorMessage =  errorMessage + "Please enter your Lastname" ;
           }
           if(user.getCity() == null) {
            errorMessage =  errorMessage + "Please enter your Living City" ;
           }
        if(user.getEmail() == null) {
            errorMessage =  errorMessage + "Please Enter your Email" ;
           }
           if(user.getPostcode() == null) {
            errorMessage =  errorMessage + "Please Enter your Postcode" ;
           }
           if(user.getTelephone() == null) {
            errorMessage =  errorMessage + "Please Enter your Telephone number" ;
           }
   
        throw new RequiredDataException(errorMessage) ;
    }

        userService.create(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PutMapping(value="/id/{id}")
    public User update(@PathVariable int id, @RequestBody User user) {
        
     
 
        try {
            user.setId(id);
            return userService.update(user);
            }
            catch (RuntimeException e) {
                throw new NotFoundIdException("ID : " + id + " not found");
            }
        
    }

   @PatchMapping(value="/partialid/{id}")
   public ResponseEntity<User> updatePartial(@PathVariable("id") Integer id, @RequestBody User userupdate) {
       
       
        try {
            Optional<User> users = userService.findUserById(id);
            User updateduser = users.get() ;
            
            if(userupdate.getUsername() != null) {
                updateduser.setUsername(userupdate.getUsername());
            }
            if(userupdate.getfirst_name() != null) {
                updateduser.setfirst_name(userupdate.getfirst_name());
            }
            if(userupdate.getlast_name() != null) {
                updateduser.setlast_name(userupdate.getlast_name());
            }
            if(userupdate.getEmail() != null) {
                updateduser.setEmail(userupdate.getEmail());
            }
            if(userupdate.getPostcode() != null) {
                updateduser.setPostcode(userupdate.getPostcode());
            }
            if(userupdate.getTelephone() != null) {
                updateduser.setTelephone(userupdate.getTelephone());
            }
            
            userService.updatePartial(updateduser) ;
       
       return new ResponseEntity<>(updateduser,HttpStatus.ACCEPTED);
            }
            catch (RuntimeException e) {
                throw new NotFoundIdException("ID : " + id + " not found");
            }
      
   }

   @DeleteMapping(value = "/id/{id}") 
   public void delete(@PathVariable("id") int id) {
       try {
       userService.delete(id);
       }
       catch (RuntimeException e) {
           throw new NotFoundIdException("ID : " + id + " not found");
       }
   }

}
