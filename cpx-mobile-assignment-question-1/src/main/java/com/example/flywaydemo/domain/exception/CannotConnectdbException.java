package com.example.flywaydemo.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class CannotConnectdbException extends RuntimeException {

    public CannotConnectdbException(String message) {
        super(message);
    
}
}