package com.example.flywaydemo.domain.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotfoundUsernameException extends RuntimeException {
    
  public NotfoundUsernameException(String message) {
    super(message);
  }
}
