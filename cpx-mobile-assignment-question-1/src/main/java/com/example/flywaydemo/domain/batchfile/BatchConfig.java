package com.example.flywaydemo.domain.batchfile;


import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.example.flywaydemo.domain.Model.User;


@Configuration
public class BatchConfig {
    @Value("userinfo.csv")
    private Resource inputFile;


    @Autowired
    private javax.sql.DataSource dataSource;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean("step1ItemReader")
    public ItemReader<User> fileReader() {
        return new FlatFileItemReaderBuilder<User>()
                .resource(new ClassPathResource("userinfo.csv"))
                .name("file-reader")
                .targetType(User.class)
                .delimited().delimiter(",")
                .names(new String[]{"id","username","first_name","last_name","email","city","postcode","telephone"})
                .build();
    }

    @Bean("step1ItemWriter")
    public ItemWriter<User> dbWriter() {
        return new JdbcBatchItemWriterBuilder<User>()
                .dataSource(dataSource)
                .sql("INSERT INTO userinfo (userid , username , first_name, last_name , email , city , postcode , telephone) VALUES (:id, :username, :first_name, :last_name, :email, :city , :postcode, :telephone)")
                .beanMapped()
                .build();
    }

    @Bean("step1ItemProcessor")
    public ItemProcessor<User, User> processor() {
        return new ItemProcessor<User, User>() {
       

            @Override
            public User process(User item) {
                String firstNameUpperCase = item.getfirst_name().toUpperCase();
                String lastnameUpperCase = item.getlast_name().toUpperCase() ;

            User updatUser = new User() ;
            updatUser.setId(item.getId());    
            updatUser.setUsername(item.getUsername());
            updatUser.setfirst_name(firstNameUpperCase);
            updatUser.setlast_name(lastnameUpperCase);
            updatUser.setCity(item.getCity());
            updatUser.setEmail(item.getEmail());
            updatUser.setPostcode(item.getPostcode());
            updatUser.setTelephone(item.getTelephone());
            System.out.println(updatUser.getCity());
                return updatUser ;
                       
            }
        };
    }

    @Bean
    public Step step1(ItemReader<User> step1ItemReader,
    ItemProcessor<User, User> step1ItemProcessor,
    ItemWriter<User> step1ItemWriter) {
return stepBuilderFactory.get("Step1 - Import User Data")
.<User, User>chunk(1000)
.reader(step1ItemReader)
.processor(step1ItemProcessor)
.writer(step1ItemWriter)
.build();
    }
}
